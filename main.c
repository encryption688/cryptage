#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>

#include "cryptage.h"
#include "function.h"

void main() {

    setlocale (LC_ALL,"fr_FR.utf8");

    //Choix pour crypter ou décrytper
    int choix;
    printf("Voulez vous : 1-Crypter         2-Décrypter         #-Arreter\n");
    scanf("%d",&choix);

    switch(choix){
        case 1:
            printf("Nom du fichier a crypter \n");
            break;
        case 2:
            printf("Nom du fichier a decryper\n");
            break;
        default:
            printf("Fin du programme\n");
            exit(EXIT_SUCCESS);
    }

    // Récupère le nom du premier fichier
    char nfic1[100];
    scanf("%s",nfic1);
    viderBuffer();

    // Récupère le nom du fichier à créer tant qu'il est différent du premier
    printf("Nom du fichier a creer\n");
    char nfic2[100];
    scanf("%s",nfic2);
    viderBuffer();
    while(strcmp(nfic2,nfic1)==0){
        printf("Veuillez choisir un nom different du fichier à crypter\n");
        scanf("%s",nfic2);
        viderBuffer();
    }

    FILE* fic;
    FILE* fic2;
    int lgLigne;
    wchar_t * lgLine;
    fic = fopen(nfic1, "r");

    if (fic != NULL) {
        fic2 = fopen(nfic2,"w");

        //Affiche les différents cryptages
        if(choix ==1){
            printf("Cryptage : 1-Cesar          2-Vigenere\n");
        }else{
            printf("Decryptage : 1-Cesar          2-Vigenere\n");
        }
		
        //Récupère le choix tant que c'est différent des choix donnés
        int choix2;
        scanf("%d",&choix2);
        while (choix2 != 1 && choix2 != 2){
            printf("Il n'y a pas d'autres méthodes. Veuillez choisir un des choix proposés.\n");
            printf("1-Cesar          2-Vigenere\n");
            scanf("%d",&choix2);
        }

        //Demande la clé
        char cle[100];
        int decalage;
        if (choix2 == 1){
            printf("Saisir decalage \n");
            scanf("%d",&decalage);
            viderBuffer();
        }else{
            printf("Saisir mot cle \n");
            scanf("%s",cle);
            viderBuffer();
        }

        //Crypter
        if(choix==1){

            wchar_t ligne[256];
            lgLine = fgetws(ligne, 256, fic);
            while(lgLine) {
                char line[256] = "";
                size_t t = wcslen(ligne)+1;
                if(desaccent(ligne,line,t)==1){

                    //Crypte
                    if(choix2==1){
                        cesar(line,decalage);
                    }else{
                        vigenere(line,cle);
                    }

                    //Ecrit dans le fichier
                    fprintf(fic2,"%s",line);
                    lgLine = fgetws(ligne, 256, fic);
                }else{
                    lgLine = NULL;
                }
            }

        //Decrypter            
        }else{

            char* ligne = NULL;
            size_t taille = 0;
            lgLigne = getline(&ligne, &taille, fic);

            //Decrypter et ecrit dans le fichier créé tant qu'il reste une ligne à décrypter
            while (lgLigne != -1) {

                char line[taille];

                strcpy(line,ligne);

                //Decrypte
                if(choix2==1){
                    deCesar(line,decalage);
                }else{
                    deVigenere(line,cle);
                }

                //Ecrit dans le fichier
                fputs(line,fic2);
                lgLigne = getline(&ligne, &taille, fic);
            }
            free(ligne);
        } 
        fclose(fic);
        fclose(fic2);
    }else{
        printf("Il n'existe aucun fichier portant le nom %s\n",nfic1 );
        remove(nfic1);
    }
    printf("Fin du programme\n");
} 