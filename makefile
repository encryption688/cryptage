GCC = gcc
SOURCES = $(wildcard *.c)
BINAIRES = $(patsubst %.c,%.o,${SOURCES})


all: main

main: ${BINAIRES}
#${GCC} cryptage.o function.o main.c -o main
	${GCC} $^ -o $@

#cryptage.o: cryptage.c 
#function.o: function.c
%.o: %.c cryptage.h
#${GCC} -c cryptage.c
#${GCC} -c function.c
	${GCC} -c $<
	
clean:
	rm main
	rm *.o
