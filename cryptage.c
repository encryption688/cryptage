#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cryptage.h"

//Ajout le decalage decal pour chaque lettre de ligne
void cesar(char ligne[], int decal) {
    int i = 0;
    while (ligne[i] != '\0') {
        decaler(ligne,i,decal,'A','Z');
        decaler(ligne,i,decal,'a','z');
        decaler(ligne,i,decal,'0','9');
    	i++;
    }
}

//Enlève le decalage decal pour chaque lettre de ligne
void deCesar(char ligne[], int decal) {
    int i = 0;
    while (ligne[i] != '\0') {
        decaler(ligne,i,-decal,'A','Z');
        decaler(ligne,i,-decal,'a','z');
        i++;
    }
}

void vigenere(char ligne[],char cle[]){
    int i = 0;
    int j = 0;
    int decal;
    while (ligne[i] != '\0') {

        //Calcul decal
	    if(cle[j] >= 'a' && cle[j]<= 'z'){
	       decal = cle[j]-'a';
	    }else if (cle[j] >= 'A' && cle[j]<= 'Z'){
	       decal = cle[j]-'A';
	    }

        decaler(ligne,i,decal,'A','Z');
        decaler(ligne,i,decal,'a','z');

     	i++;
	    j++;
	    if(j>= strlen(cle)){
	       j=0;
	    }
    }
}

void deVigenere(char ligne[],char cle[]){
    int i = 0;
    int j = 0;
    int decal;
    while (ligne[i] != '\0') {
        if(cle[j] >= 'a' && cle[j]<= 'z'){
            decal = cle[j]-'a';
        }else if (cle[j] >= 'A' && cle[j]<= 'Z'){
            decal = cle[j]-'A';
        }

        decaler(ligne,i,-decal,'A','Z');
        decaler(ligne,i,-decal,'a','z');

        i++;
        j++;
        if(j>= strlen(cle)){
            j=0;
        }
    }
}

void decaler(char *ligne, int indice, int cle, int borneMin, int borneMax){
    if (ligne[indice] >= borneMin && ligne[indice]<= borneMax) {
        int c = ligne[indice] - borneMin;
        c += cle;
        c = c % 26;
        if(c<0){
            c+=26;
        }
        ligne[indice] = c + borneMin;
    }
}