#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <math.h>
#include "function.h"

//';' token
#define ACCENTS L"ÀÂÄÇÈÉÊËÎÏÔÖÙÛÜàâäçèéêëîïôöùûüÿŸ"
#define REPLACE  "AAACEEEEIIOOUUUaaaceeeeiioouuuyY"
#define CARACSPE L"><#({[]})/*.-@&~;:$=+/_^`|"

void viderBuffer(void){
    int c = 0;
    while (c != '\n' && c != EOF)
    {
        c = getchar();
    }
}


/*  Verifie l'alphanumérique de ligne
    Si correct, on retourne 1
    Sinon on retourne 0    
*/
int verifAlpha(wchar_t ligne[]){

    //Pour chaque caractère de ligne rechercher si c'est un caractère spécial

    for(int i=0; i<wcslen(ligne);i++){

        for(int j=0; j<wcslen(CARACSPE);j++){
            if(ligne[i]==CARACSPE[j]){
                printf("Texte eronne\n");
                return 0;
            }
        }
    }
    return 1;
}


/*  Copie ligne en char et sans les accents dans line si verifAlpha(ligne)==1
    Si correct alors on désaccentue la ligne et retourne 1
    Sinon on retourne 0    
*/
int desaccent(wchar_t ligne[], char line[], int t){

    if(verifAlpha(ligne)==1){

        //Pour chaque lettre de ligne, rechercher si c'est une lettre accentuée
        for(int i=0; i<wcslen(ligne);i++){

            /*  Recherche du caractère ligne[i] dans ACCENTS
                Si trouvé, ajout dans line[i] du caractère en position trouvé de REPLACE (en char)
                Sinon ajout dans line[i] du caractère en char et non en wchar_t
            */
            int debut=0;
            int fin = wcslen(ACCENTS)-1;
            int trouve = 0;
            int mil;
            while(trouve !=1 && debut<=fin){
                mil = floor((debut+fin)/2);
                if (ACCENTS[mil]==ligne[i]){
                    trouve = 1;
                }else{
                    if(ligne[i]>ACCENTS[mil]){
                        debut = mil + 1;
                    }else{
                        fin = mil - 1;
                    }
                }
            }

            if(trouve == 1){
                line[i]= REPLACE[mil];
            }else{
                if(debut == fin){
                    line[i]= REPLACE[fin];
                }else{

                    //Ajout dans line[i] du caractère en char et non en wchar_t
                     if (wctomb(line+i, ligne[i]) != 1)
                        return 0;
                }   
            }
        }
        return 1;
    }
    return 0;
}